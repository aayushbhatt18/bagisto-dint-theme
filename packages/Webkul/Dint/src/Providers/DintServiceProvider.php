<?php
namespace Webkul\Dint\Providers;

use Illuminate\Support\ServiceProvider;

/**
* Dint service provider
*
* @author    Aayush Bhatt <aayush.bhatt172@webkul.com>
* @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
*/
class DintServiceProvider extends ServiceProvider
{
   /**
   * Bootstrap services.
   *
   * @return void
   */
   public function boot()
   {
    $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'dint');

    $this->publishes([
      __DIR__ . '/../publishable/assets' => public_path('themes/dint/assets'),
    ], 'public');

    $this->publishes([
      __DIR__ . '/../Resources/views/shop' => resource_path('themes/dint/views'),
    ]);

   }

   /**
   * Register services.
   *
   * @return void
   */
   public function register()
   {

   }
}